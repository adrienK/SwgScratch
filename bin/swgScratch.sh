#!/bin/bash
# Copyright 2018 Iglou.eu
# Copyright 2018 Adrien Kara
# license that can be found in the LICENSE file.

# VAR
scrDump=$(cat ${0})

# FUNCTIONS
function err {
    echo "FATAL ERROR: ${1}"
    exit 1
}

function get_section {
    rqSection=$(echo "${scrDump#*#${1}_SECTION}")
    rqSection=$(echo "${rqSection%#${1}_END*}")

    echo "$rqSection"
}

function dp_help {
    get_section HELP
    exit 1
}

function init_project {
    targetRoot="${target%/*}/"

    if [[ -e ${target} ]]; then
        err "Le dossier \"${target}\" existe déja"
    elif [[ ! -d ${targetRoot} ]]; then
        err "\"${targetRoot}\" n'est pas un dossier"
    elif [[ ! -w ${targetRoot} ]]; then
        err "Impossible d'écrire dans \"${targetRoot}\""
    fi

    mkdir -p "${target}/dev"
    mkdir -p "${target}/editme"
    mkdir -p "${target}/rendered/js"
    mkdir -p "${target}/rendered/css"
    mkdir -p "${target}/rendered/media"
    
    touch "${target}/rendered/robots.txt"
    touch "${target}/rendered/sitemap.xml"
    
    get_section CONF > "${target}/conf"
}

function build_project {
    targetRoot="${target%/*}/"

    if [[ ! -w "${target}/rendered" ]]; then
        err "Impossible d'écrire dans \"${targetRoot}/rendered\""
    elif [[ ! -r "${target}/conf" ]]; then
        err "Impossible de lire la conf \"${targetRoot}/conf\""
    fi

    find "${target}/rendered" -name "*.html" -exec rm {} \;

    fileList=""
    while IFS= read -d '' -r file; do
        if [[ "${target}/editme" = ${file} ]]; then
            continue
        else
            fileList="${fileList}${file#*editme/}\n"
        fi
    done < <(find "${target}/editme" -print0)
    fileList=$(printf "${fileList}")

    mv ${target}/conf ${target}/conf.back
    echo "pageBuildNav=\"$(build_menu "${target}/editme/")\"" >> ${target}/conf
    echo "pageBuildLinks=\"$(jsCss_links)\"" >> ${target}/conf
    cat ${target}/conf.back >> ${target}/conf

    . ${target}/conf "" ""

    printf "User-agent: *\nAllow: /\n${cf_RobotsTxt}\nSitemap: ${cf_siteUrl}/sitemap.xml" > "${target}/rendered/robots.txt"

    for page in ${fileList}; do
        if [[ -d "${target}/editme/${page}" ]]; then
            mkdir -p "${target}/rendered/${page}"
            continue
        fi

        pageBuildName="$(echo ${page##*/} | sed 's/_/ /g')"

        pageBuildArticle="$(cat ${target}/editme/${page})"
        for var in $(echo "${pageBuildArticle}" | grep -o "\${\w*}" | sed 's/[\${}]//g'); do
            if [[ -z ${!var} ]]; then
                continue
            fi

            cleanVar=$(echo "${!var}" | sed 's/\//\\\//g')
            pageBuildArticle=$(echo "${pageBuildArticle}" | sed "s/\${${var}}/${cleanVar}/g")
        done

        if [[ -z $extraTextRender ]]; then
            pageBuildArticle="$(echo "${pageBuildArticle}")"
        else
            pageBuildArticle="$(echo "${pageBuildArticle}" | ${extraTextRender})"
        fi
        . ${target}/conf "${pageBuildName}" "${pageBuildArticle}"

        echo "${cf_htmlBody}" > "${target}/rendered/${page}.html"
    done

    mv ${target}/conf.back ${target}/conf
}

function extract_var {
    page="${1}"
    
    for var in $(echo "${page}" | grep -o "\${\w*}" | sed 's/[\${}]//g'); do
        if [[ -z ${!var} ]]; then
            continue
        fi

        page=$(echo "${page}" | sed "s/\${${var}}/${!var}/g")
    done

    echo "${page}"
}

function jsCss_links {
    out=""

    while IFS= read -d '' -r file; do
        out=$(printf "${out}<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"/css/${file##*/}\\\" />\n")
    done < <(find "${target}/rendered/css" -name "*.css" -print0)

    while IFS= read -d '' -r file; do
        out=$(printf "${out}<script type="text/javascript" src=\\\"/js/${file##*/}\\\"></script>\n")
    done < <(find "${target}/rendered/js" -name "*.js" -print0)

    echo ${out}
}

function build_menu {
	if [[ -z ${2} ]];then
    	lvl=0
    else
    	lvl=${2}
    fi
    
    out="<ul class=\\\"menu lvl${lvl}\\\">"
    for entry in $(find "${1}" -maxdepth 1); do
        if [[ -d ${entry} ]]; then
            if [[ ${1} = ${entry} ]]; then
                continue
            fi
			lvl=$((${lvl} + 1))
            out="${out}<li class=\\\"menu slave lvl${lvl}\\\"><span>$(echo ${entry##*/} | sed 's/_/ /g')</span>$(build_menu "${entry}" "${lvl}")</li>"
        else
            entry=${entry#*editme}
            out="${out}<li><a href=\\\"${entry}.html\\\">$(echo ${entry##*/} | sed 's/_/ /g')</a></li>"
        fi
    done

    echo "${out}</ul>"
}

function send_project {
    if [[ ! -r "${target}/conf" ]]; then
        err "Impossible de lire la conf \"${targetRoot}/conf\""
    fi

    . ${target}/conf "" ""

    if [[ -z ${scp_user} ]];then
        err "Pas d'utilisateur idiqué dans la conf scp"
    elif [[ -z ${scp_server} ]];then
        err "Pas de serveur indiqué dans la conf scp"
    elif [[ -z ${scp_distDir} ]];then
        err "Pas de dossier distant indiqué dans la conf scp"
    fi

    scp -r -P ${scp_port:-22} ${target}/rendered/* ${scp_user}@${scp_server}:${scp_distDir}
}

# MAIN
if [ 2 -ne $# ]; then
    dp_help
fi

typeset -r request="${1}"
typeset -r target="${2}"

case "${request}" in
    "init"|"INIT")
        init_project
        ;;
    "build"|"BUILD")
        build_project
        ;;
    "scp"|"SCP")
        send_project
        ;;
    *)
        dp_help
        ;;
esac

exit 0

#CONF_SECTION
pageBuildName="${1}"
pageBuildArticle="${2}"

# $(echo {} | ${extraTextRender})
extraTextRender=""

scp_distDir=""
scp_server=""
scp_user=""
scp_port=""

cf_appName="${target#*/}"
cf_siteUrl="https://${cf_appName}/"
cf_Title="${cf_appName}"
cf_description=""
cf_keywords=""
cf_footer=""

cf_lang="fr_FR"
cf_logo=""
cf_themeColor="#5E62B6"

cf_RobotsTxt=""
cf_genSiteMap=""

cf_cssBody="
<style>
body{display:flex;flex-wrap:wrap;margin:0 2%;background-color:${cf_themeColor};font-family:Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;word-wrap:break-word;font-size:16px;}
header{padding:15px 0}
header a{color:white;text-transform:capitalize;font-family:Arial,sans-serif;text-decoration: none;font-size:2em;height: 100%;display:flex;align-items:center;}
nav{flex:1;display:flex;align-items:center;flex-direction:row-reverse;}
nav ul{list-style:none;}
nav ul li{display:inline-block;padding-left:15px;}
nav a,nav span{text-decoration:none;font-weight:bold;text-transform:capitalize;color:white;}
nav a:hover{border-bottom:2px solid}
article{flex:1 100%;background:white;padding:15px 5%;border-radius:3px;box-shadow:0 0 3px black;}
footer{flex:1 100%;padding:15px 0;text-align:right;}
h1,h2,h3,h4,h5,h6{margin-top:0;min-height:1rem;font-weight:700;}
h1{padding-bottom:.3em;font-size:2.25em;line-height:1.2;border-bottom:1px solid #eee;margin-bottom:16px;padding:0;}
h2{padding-bottom:.3em;font-size:1.75em;line-height:1.225;border-bottom:1px solid #eee;}
h3{font-size:1.5em;line-height:1.43;}
blockquote{padding:1px 15px;margin:0;color:#777;border-left:4px solid #ddd;line-height:0;}
pre{padding:16px;overflow:auto;font-size:85%;line-height:1.45;background-color:#f7f7f7;border-radius:3px;word-wrap:normal;}
code{word-wrap:normal;font-size:100%;word-break:normal;}
img{max-width:100%;}
.menu.slave ul{display:none;position:absolute;padding:10px 5px;background:${cf_themeColor};}
.menu.slave:hover ul{display:flex;flex-direction:column-reverse;}
.menu.slave:hover ul li{padding:5px 0;}
.menu.slave span::after{content:\" \25BF\";}</style>
"
cf_htmlBody="
<!doctype html>
<html prefix=\"og: http://ogp.me/ns#\" lang=fr>
<head>
<title>${cf_appName} - ${pageBuildName}</title>
<meta charset=UTF-8>
<meta http-equiv=X-UA-Compatible content=\"IE=edge\" />
<meta http-equiv=last-modified content=\"$(date +%F)\" />
<meta name=viewport content=\"width=device-width, initial-scale=1.0, user-scalable=yes, viewport-fit=cover\" />
<meta name=application-name content=\"${cf_appName}\" />
<meta name=description content=\"${cf_description}\" />
<meta name=keywords  content=\"${cf_keywords}\" />
<meta name=theme-color content=\"${cf_themeColor}\" />
<meta name=msapplication-TileColor content=\"${cf_themeColor}\" />
<meta name=msapplication-TileImage content=\"/media/${cf_logo}\" />
<meta property=og:site_name content=\"${cf_appName}\" />
<meta property=og:locale content=\"${cf_lang}\" />
<meta property=og:type content=website />
<meta property=og:url content=\"${cf_siteUrl}\" />
<meta property=og:title content=\"${cf_appName} - ${pageBuildName}\" />
<meta property=og:description content=\"${cf_description}\" />
<meta property=og:image content=\"/media/${cf_logo}\" />
<meta name=generator content=\"swgScratch\" />
<link rel=icon type=\"image/png\" href=\"/media/${cf_logo}\" />
${cf_cssBody}
${pageBuildLinks}
</head>
<body>
<header><a href=\"/\">${cf_Title}</a></header>
<nav>${pageBuildNav}</nav>
<article>
${pageBuildArticle}
</article>
<footer>${cf_footer}</footer>
"
#CONF_END
#HELP_SECTION
Usage: swgScratch ACTION... PROJET...
Aide a la création de site statique

Il doit avoir strictement 2args,
l'action a effectuer suivie directement du /répertoire/projet

Liste des actions:
INIT    Pour initialiser le projet
BUILD   Pour contruire le rendu html du projet
SCP     Pour envoyer le rendu sur un serveur

Le chemin du projet,
doit cibler un dossier avec les droits en ecriture
et ce terminer par le nom du projet

Ex:
swgScratch INIT ~/djob/monSuperProjet
swgScratch BUILD ~/djob/monSuperProjet

Pour plus d'informations:
https://git.iglou.eu/Laboratory/SwgScratch
#HELP_END

