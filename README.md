# ![logo](https://git.iglou.eu/attachments/45603c77-1bc7-429b-852a-17604a44de69) SwgScratch
**S**tatic **W**ebsite **G**enerator from **Scratch**

**SwgScratch** est un générateur de site statique qui s'utilise en ligne de commande.    
Réalisé pour répondre a des besoins rapides mais simple, pour rester [KISS](https://fr.wikipedia.org/wiki/Principe_KISS) et economiser des ressources serveur.

Il ne nécésite pas de compétences particuliéres, n'a pas besoin d'installer de dépendances,    
les programmes utilisé sont embarqué from scratch dans la majoritée des Os, le theme de base est valide W3c et firefox book mode.

Fonctionne sous, Gnu/Linux, BSD\*, OsX\*, ...    
Testé uniquement avec bash, mais devrait tourner avec toute la famille Korn ([*voir compatibility test*](http://www.tablespace.net/papers/shellsheet.html) )    
*Il est possible qu'il tourne aussi sous Windows, mais c'est pas moi qui vais tester ça ...*

## :rocket: **HOW TO** 
*Pour l'exemple le nom du projet est "fatcow.com"*

Les pages du sites peuvent étre rédigé dans tous les formats,   
Si la conf "extraTextRender" est vide, la page sera considéré comme rédigé en HTML,    
Si non, "extraTextRender" est chaîné (pipe) sous cette forme "echo $page | $extraTextRender"

### Utilisation:
* Initialisation du nouveau "projet"
`$ swgScratch INIT ~/boulot/fatcow.com`

* Configuration et rédaction des pages
`$ vim ~/boulot/fatcow.com/conf`
`$ vim ~/boulot/fatcow.com/editme/ma_super_page`

* Construction du projet
`$ swgScratch BUILD ~/boulot/fatcow.com`

* Envoi du projet sur le serveur
`$ swgScratch SCP ~/boulot/fatcow.com`

### Arguments:
L'argument **INIT** permet d'initialiser le projet de site.
Il va créer les fichiers **"conf"**, **"robots.txt"**, **"sitemap.xml"** et les dossiers:
* **"dev"**, dossier destiné aux traveaux d'image, croquis, notes ...
* **"editme"**, LE dossier dans le quel on réalise ses pages et son arborecence
* **"rendered"**, là où le rendu vas finir, il est lui même initialiser avec 3 autres dossiers "css" (pour les fichiers css), "js" (pour les cripts js) et "media" (pour les medias ...)

L'argument **BUILD** va contruire les pages avec les variables définies dans le fichier **"conf"** et les pages présentes dans le dossier **"editme"**, afin d'en construire la sortie dans **"rendered"**. Il va aussi au passage inclure les css et js si il y en a et contruire le menu.

Pour finir, l'argument **SCP**, va utiliser les variables **scp_\*** du fichier **"conf"** pour envoyer le resultat vers un serveur distant via la commande scp.

### */conf:
**Variables utilisé pour effectuer le rendu**
>pageBuildName="${1}"
pageBuildArticle="${2}"

**Varibles pour le theme de base**
*a modifier si besoin de changer la structure*
*préférez un fichier \*/rendered/\*.css et l'utilisation de flex*
>cf_cssBody=""
cf_htmlBody=""

**Pour utiliser un outil externe pour le rendu**
*ex: extraTextRender="markdown"*
*va étre executé comme $(echo {} | markdown) > \*.html*
>extraTextRender=""

**Variables pour configurer scp**
>scp_distDir="" # /dir/sur/le/serveur
scp_server="" # 127.0.0.1
scp_user="" # utilisateur du serveur
scp_port="" # si on utilise pas le port 22

**Variables globales pour le site**
>cf_appName="${target#*/}" # Utilise par default le nom du projet "fatcow.com"
cf_siteUrl="https://${cf_appName}/" # ""
cf_Title="${cf_appName}" # ""
cf_description=""
cf_keywords=""
cf_footer=""

>cf_lang="fr_FR"
cf_logo=""
cf_themeColor="#5E62B6"

>cf_RobotsTxt="" # Pour ajouter des entrées/exclusions
cf_genSiteMap="" # Pas utilisé pour le moment

## :construction: **INSTALLATION** 
**Required:** cat, sed, mkdir, mv, touch, find, scp, date    
**Tested on:** ArchLinux, Debian et Ubuntu sur x86_64    

1. Télécharger/cloner le script
2. Le rendre executable *chmod +x swgScratch*
3. Utiliser ...

> Peut étre envoyé dans /usr/bin pour l'utiliser comme un binaire

## :shipit: **DISCLAIMER**
This program is under MIT license [[read the entire license file]](https://git.iglou.eu/Laboratory/SwgScratch/raw/branch/master/LICENSE)

---